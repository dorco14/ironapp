const express = require('express')
const router = express.Router()
const usersModel = require('../Models/userModel')

router.get('/users', async (req, res) => {
    res.send(await usersModel.find());
});

module.exports = router