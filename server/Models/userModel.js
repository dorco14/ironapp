var mongoose = require('mongoose');
var usersCollectionName = 'users'
var usersSchema = new mongoose.Schema({
  _id: {
    type: String,
  },
  name: String,
});

var userModel = mongoose.model(usersCollectionName, usersSchema);
module.exports = userModel;