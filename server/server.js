const express = require('express')
const app = express()
const port = 3000
const dbConnection = require('./dbConnection')
const userController = require('./Controllers/usersController')
const cors = require('cors');


dbConnection.connect();
app.use(cors());
app.get('/', (req, res) => res.send('Hello World!'))
app.use(userController)
app.listen(port, () => console.log(`Example app listening on port ${port}!`))