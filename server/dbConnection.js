const mongoose = require('mongoose')
const dbHost = 'mongodb+srv://yossi:Aa123456@mongodatacluster-cq949.mongodb.net/test?retryWrites=true&w=majority'

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName: 'IronApp'
}

const connect = () => {
    mongoose.connect(dbHost, options);
    mongoose.connection.on('error', (err) => {
        setTimeout(() => mongoose.connect(dbHost, options), 10000)
    });
}

module.exports = { connect }