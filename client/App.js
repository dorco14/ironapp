import React, { useEffect } from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { isMountedRef, navigationRef } from './RootNavigation';
import { ThemeProvider } from 'react-native-elements';
import Navbar from './components/Navbar';

import DrinksScreen from './screens/drinks';
import FoodScreen from './screens/food';
import DohEhadScreen from './screens/dohehad';
import ExitsScreen from './screens/exits';

// Get the icons name from https://infinitered.github.io/ionicons-version-3-search/
const linksMetadata = [
  {
    title: 'יציאות',
    icon: "ios-beer",
    component: "Exits"
  },
  {
    title: 'אוכל',
    icon: "ios-restaurant",
    component: "Food"
  },
  {
    title: 'שתייה',
    icon: "ios-cafe",
    component: "Drinks"
  },
  {
    title: 'נוכחות',
    icon: "ios-calendar",
    component: "DohEhad"
  }
];

const Stack = createStackNavigator();

export default function App() {
  useEffect(() => {
    isMountedRef.current = true;

    return () => (isMountedRef.current = false);
  });

  return (
    <ThemeProvider>
      <View style={styles.container}>
        <View id='content' style={styles.content}>
          <NavigationContainer ref={navigationRef}>
            <Stack.Navigator>
              <Stack.Screen name="Drinks" component={DrinksScreen} />
              <Stack.Screen name="Food" component={FoodScreen} />
              <Stack.Screen name="DohEhad" component={DohEhadScreen} />
              <Stack.Screen name="Exits" component={ExitsScreen} />
            </Stack.Navigator>
          </NavigationContainer>
        </View>
        <View id='navbar' style={styles.navbar}>
          <Navbar linksMetadata={linksMetadata} />
        </View>
      </View>
    </ThemeProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    direction: 'rtl'
  },
  content: {
    flex: 10
  },
  navbar: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopWidth: 2,
    borderTopColor: 'lightgrey'
  }
});
