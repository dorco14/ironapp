import axios from 'axios';

async function getUsers() {
    try {
        const { data } = await axios.get('http://192.168.1.19:3000/users');
        console.log(data)
        return data;
    } catch(err) {
        console.log('error getting users');
        console.log(err);
        return [];
    }
}

export {
    getUsers
};