import React, { Fragment } from 'react';
import { StyleSheet, Text, View, TextInput, FlatList, Picker } from 'react-native';
import { CheckBox, Input, Header } from 'react-native-elements';
import { Dropdown } from 'react-native-material-dropdown';

export default function MainPage() {
    return (
        <Fragment>
            <Header
                centerComponent={{ text: 'בוקר טוב חרבן', style: { color: '#fff' } }}
            />
            <Text h1>אח/ות יקר/ה את/ה מגיע היום?</Text>
            <View style={styles.viewStyle}>
                <CheckBox
                    title='נוכח'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    // checked={this.state.checked}
                    // onPress={() => this.setState({ checked: !this.state.checked })}
                />
                <CheckBox
                    title='לא נוכח'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                />
            </View>
            <Input
                placeholder='הסבר נמק פרט והבא דוגמות'
            />
            <Text h1>שותה משהו?</Text>
            <View style={styles.viewStyle}>
                <CheckBox
                    title='בטח שותה'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                />
                <CheckBox
                    title='לא תודה כפרה'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                />
            </View>
            <Dropdown style={styles.dropdownStyle}
                label='מה שותה?'
                data={[{
                    value: 'קפה',
                }, {
                    value: 'תה',
                }]}
            />
            <Dropdown style={styles.dropdownStyle}
                label='כמה סוכר?'
                data={[{
                    value: '0',
                }, {
                    value: '0.5',
                }, {
                    value: '1',
                }, {
                    value: '1.5',
                }, {
                    value: '2',
                }, {
                    value: '2.5',
                }, {
                    value: '3',
                }]}
            />
            <Dropdown style={styles.dropdownStyle}
                label='סוג קפה'
                data={[{
                    value: 'מגורען',
                }, {
                    value: 'מגורען ללא קפאין',
                }, {
                    value: 'עלית',
                }, {
                    value: 'עלית וניל',
                }]}
            />
            <Dropdown style={styles.dropdownStyle}
                label='סוג חלב'
                data={[{
                    value: 'ללא חלב',
                }, {
                    value: '3%',
                }, {
                    value: '1%',
                }, {
                    value: 'סויה',
                }, {
                    value: 'סויה לייט',
                }, {
                    value: 'שקדים',
                }]}
            />
            <Dropdown style={styles.dropdownStyle}
                label='גודל'
                data={[{
                    value: 'S',
                }, {
                    value: 'M',
                }, {
                    value: 'L',
                }]}
            />
            <Text h1>ומה עם אוכל לצהריים?</Text>
            <View style={styles.viewStyle}>
                <CheckBox
                    title='הבאתי מהבית'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                />
                <CheckBox
                    title='נמצא משהו'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                />
            </View>
        </Fragment>
    );
}

const styles = StyleSheet.create({
    viewStyle: {
        display: 'flex',
        flexDirection: 'row'
    },
    viewStyles: {
        display: 'flex',
        flexDirection: 'column'
    },
    container: {
        flex: 1,
        paddingTop: 22
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44
    },
    dropdownStyle: {
        padding: 50,
        fontSize: 18
    }
});

