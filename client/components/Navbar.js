import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import { navigationRef } from '../RootNavigation';
import Icon from 'react-native-vector-icons/Ionicons';

function Navbar({ linksMetadata }) {
    return (
        <View style={styles.navbar}>
            {
                linksMetadata.map((linkMetadata, index) => (
                    <TouchableOpacity
                        onPress={() => navigationRef.current.navigate(linkMetadata.component)}
                        key={index}
                        style={styles.navbarTab}
                    >
                        <Icon style={styles.navbarText} name={linkMetadata.icon} size={30}/>
                        <Text style={styles.navbarText}>{linkMetadata.title}</Text>
                    </TouchableOpacity>
                ))
            }
        </View>
    );
}

const styles = StyleSheet.create({
    navbar: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-around',
    },
    navbarTab: {
        justifyContent: 'center',
        flex: 1,
        borderWidth: 0.5,
        borderColor: 'lightgray'
    },
    navbarText: {
        textAlign: 'center',
        color: 'gray'
    }
});

export default Navbar;