import React, { useState, useEffect } from 'react';
import { View, ScrollView, Text, StyleSheet } from 'react-native';
import { ListItem } from 'react-native-elements'
import { getUsers } from '../services/users';

export default function DrinksScreen() {

    const [users, setUsers] = useState(null);
    const fetchUsers = async () => {
        const data = await getUsers();
        return data;
    };

    useEffect(() => {
        fetchUsers().then((users) => setUsers(users));
    });

    return (
        <View>
            {
                !users ?
                    <Text>טוען מידע...</Text> :
                    <ScrollView>
                        {
                            users.map((user, index) => (
                                <ListItem
                                    style={StyleSheet.listItem}
                                    key={index}
                                    leftAvatar={{ source: { uri: user.image } }}
                                    title={user.userName}
                                    subtitle={user.descdrink}
                                    bottomDivider
                                    badge={{
                                        value: `${user.drink}`,
                                        textStyle: { color: 'white' },
                                        badgeStyle: {backgroundColor:'gray'}
                                    }}
                                />
                            ))
                        }
                    </ScrollView>
            }
        </View>
    );
}